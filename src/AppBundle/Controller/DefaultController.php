<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\User;
use AppBundle\Entity\UserVisit;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class DefaultController extends Controller
{

    /**
     * * ### Minimal Response (e.g. anonymous) ###
     *
     *     {
     *       "id": 999,
     *       "login": "test",
     *       "name": "testname"
     *     }<br><br>
     *     {
     *       "errors" : {
     *          "login" : [
     *          "Some error"
     *      ]
     *      }
     *     }
     * @ApiDoc(
     *  description="Create new User Object",
     *  parameters={
     *      {"name"="login", "dataType"="string", "required"=true, "description"="User login"},
     *      {"name"="name", "dataType"="string", "required"=true, "description"="User name"}
     *  },
     *  statusCodes={
     *      200="Returned when successful",
     *      400="Returned when validation failed",
     *  }
     * )
     * @Route("/user")
     * @Method("POST")
     */
    public function makeAction(Request $request)
    {
        $user = new User();
        $request = $request->request;
        $user->setLogin($request->get('login'));
        $user->setName($request->get('name'));

        $errors = $this->get('validator')->validate($user);

        if (count($errors)) {
            return new JsonResponse(['errors' => $this->prepareErrors($errors)], 400);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        return new JsonResponse([
            'id' => $user->getId(),
            'login' => $user->getLogin(),
            'name' => $user->getName()
        ]);
    }

    /**
     *
     * * ### Minimal Response (e.g. anonymous) ###
     *
     *     {
     *       "id": 999,
     *       "login": "test",
     *       "name": "testname"
     *     }
     *
     * @ApiDoc(
     *  description="Get User Object",
     *  requirements={
     *      {
     *        "name"="id",
     *        "dataType"="integer",
     *        "requirement"="\d+",
     *        "description"="User id"
     *      }
     *  },
     *  statusCodes={
     *      200="Returned when successful",
     *      404="Returned when the user is not found"
     *  }
     * )
     * @Route("/user/{id}", requirements={"id": "\d+"})
     * @Method("GET")
     */
    public function readAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->find($id);

        if ($user) {
            return new JsonResponse([
                'id' => $user->getId(),
                'login' => $user->getLogin(),
                'name' => $user->getName()
            ]);
        }

        return new JsonResponse(null, 404);
    }

    /**
     * * ### Minimal Response (e.g. anonymous) ###
     *
     *     {
     *       "id": 999,
     *       "login": "test",
     *       "name": "testname"
     *     }<br><br>
     *     {
     *       "errors" : {
     *          "login" : [
     *          "Some error"
     *      ]
     *      }
     *
     * @ApiDoc(
     *  description="Update User Object",
     *  requirements={
     *      {
     *        "name"="id",
     *        "dataType"="integer",
     *        "requirement"="\d+",
     *        "description"="User id"
     *      }
     *  },
     *  parameters={
     *      {"name"="login", "dataType"="string", "required"=true, "description"="User login"},
     *      {"name"="name", "dataType"="string", "required"=true, "description"="User name"}
     *  },
     *  statusCodes={
     *      200="Returned when successful",
     *      400="Returned when validation failed",
     *      404="Returned when the user is not found"
     *  }
     * )
     * @Route("/user/{id}", requirements={"id": "\d+"})
     * @Method("PUT")
     */
    public function updateAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->find($id);

        if (!$user) {
            return new JsonResponse(null, 404);
        }

        $request = $request->request;
        $user->setLogin($request->get('login'));
        $user->setName($request->get('name'));

        $errors = $this->get('validator')->validate($user);

        if (count($errors)) {
            return new JsonResponse(['errors' => $this->prepareErrors($errors)], 400);
        }

        $em->persist($user);
        $em->flush();

        return new JsonResponse(['id' => $user->getId()]);
    }

    /**
     * * ### Minimal Response (e.g. anonymous) ###
     *
     *     {
     *
     *     }
     * @ApiDoc(
     *  description="Remove User Object",
     *  requirements={
     *      {
     *        "name"="id",
     *        "dataType"="integer",
     *        "requirement"="\d+",
     *        "description"="User id"
     *      }
     *  },
     *  statusCodes={
     *      200="Returned when successful",
     *      404="Returned when the user is not found"
     *  }
     * )
     * @Route("/user/{id}", requirements={"id": "\d+"})
     * @Method("DELETE")
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->find($id);

        if (!$user) {
            return new JsonResponse(null, 404);
        }

        $em->remove($user);
        $em->flush();

        return new JsonResponse();
    }

    /**
     * * ### Minimal Response (e.g. anonymous) ###
     *
     *     {
     *
     *     }
     *
     * @ApiDoc(
     *  description="Add last user activity",
     *  requirements={
     *      {
     *        "name"="id",
     *        "dataType"="integer",
     *        "requirement"="\d+",
     *        "description"="User id"
     *      }
     *  },
     *  statusCodes={
     *      200="Returned when successful",
     *      404="Returned when the user is not found"
     *  }
     * )
     * @Route("/visit/{id}", requirements={"id": "\d+"})
     * @Method("POST")
     */
    public function visitAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->find($id);

        if (!$user) {
            throw new HttpException(404, 'User not found');
        }

        $userVisit = new UserVisit();
        $userVisit->setUserId($user->getId());
        $userVisit->setTime(time());

        $em->persist($userVisit);
        $em->flush();

        return new JsonResponse();
    }

    /**
     *
     * * ### Minimal Response (e.g. anonymous) ###
     *
     *     {
     *         "count" : 10
     *     }
     *
     * @ApiDoc(
     *  description="Find active users count in time range",
     *  requirements={
     *      {
     *        "name"="start",
     *        "dataType"="integer",
     *        "requirement"="\d+",
     *        "description"="From timestamp"
     *      },
     *     {
     *        "name"="end",
     *        "dataType"="integer",
     *        "requirement"="\d+",
     *        "description"="To timestamp"
     *      }
     *  },
     *  statusCodes={
     *      200="Returned when successful",
     *  }
     * )
     * @Route("/daily-active-users/{start}/{end}", requirements={"start": "\d+", "end": "\d+"})
     * @Method("GET")
     */
    public function countAction($start, $end)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            'SELECT count(a.id) cnt
             FROM AppBundle:User a, AppBundle:UserVisit b
             WHERE a.id = b.user_id AND b.time BETWEEN :start AND :end
             '
        )->setParameter('start', $start)
        ->setParameter('end', $end);

        $usersCnt = $query->getSingleScalarResult();

        return new JsonResponse(['count' => $usersCnt]);
    }

    private static function prepareErrors($errors)
    {
        $res = [];
        foreach ($errors as $error) {
            $res[$error->getPropertyPath()][] = $error->getMessage();
        }
        return $res;
    }
}
