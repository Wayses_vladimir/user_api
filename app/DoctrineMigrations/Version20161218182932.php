<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161218182932 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $table = $schema->createTable('users_visits');
        $table->addColumn('id', 'integer', ['autoincrement' => true,]);
        $table->addColumn('user_id', 'integer');
        $table->addColumn('time', 'integer');
        $table->setPrimaryKey(['id']);

        $table->addForeignKeyConstraint('users', ['user_id'], ['id']);

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $schema->dropTable('users_visits');
    }
}
